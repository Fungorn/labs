#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>

using namespace std;

vector<int> prefix_func(const string &s) {
	vector<int> p(s.length());

	int k = 0;
	p[0] = 0;
	for (int i = 1; i < s.length(); ++i) {
		while (k > 0 && s[k] != s[i]) {
			k = p[k - 1]; 
		}
		if (s[k] == s[i]) {
			++k;
		}
		p[i] = k;
	}
	return p;
}

int find(string &s, string &t) {
	if (s.length() < t.length()) {
		return -1;
	}

	if (!t.length()) {
		return s.length(); //substr len =0
	}
	//?
	typedef unordered_map<char, int> TStopTable; // Таблица смещений
	typedef unordered_map<int, int> TSufficsTable;
	TStopTable stop_table;
	TSufficsTable suffics_table;

	for (int i = 0; i < t.length(); ++i) {
		stop_table[t[i]] = i;
	}

	string rt(t.rbegin(), t.rend()); //reverse t
	vector<int> p = prefix_func(t), pr = prefix_func(rt);
	for (int i = 0; i < t.length() + 1; ++i) {
		suffics_table[i] = t.length() - p.back();
	}

	for (int i = 1; i < t.length(); ++i) {
		int j = pr[i];
		suffics_table[j] = min(suffics_table[j], i - pr[i] + 1);
	}
 
	for (int shift = 0; shift <= s.length() - t.length(); ) {
		
		int pos = t.length() - 1;
		cout<<shift+t.length()<<' ';
		//cout<<s[shift+t.length()-1]<<' ';
		while (t[pos] == s[pos + shift]) {
			if (pos == 0) {
				return shift;
				} //sravn stroki i podstroki
			--pos;
			cout<<pos+shift+1<<' ';
		}
		
		if (pos == t.length() - 1) {
			TStopTable::const_iterator stop_symbol = stop_table.find(s[pos + shift]);
			int stop_symbol_additional = pos - (stop_symbol != stop_table.end() ? stop_symbol->second : -1);
			shift += stop_symbol_additional;
		} else {
			shift += suffics_table[t.length() - pos - 1];
		}
	}

	return -1;
}

int main() 
{
	string str="this is simple example";
	string substr="example";
	// string str = "strstr strokashechka"; // Debug, lol
	// string substr = "stroka";
	// std::vector<int> v=prefix_func(substr);
	// string rt(substr.rbegin(), substr.rend());
	// std::vector<int> r=prefix_func(rt);
	// for (std::vector<int>::iterator it = v.begin() ; it != v.end(); ++it)
	// 	cout<<' '<<*it;
	// cout<<endl;
	// for (std::vector<int>::iterator it = r.begin() ; it != r.end(); ++it)
	// 	cout<<' '<<*it;
	find(str, substr);
	return 0;
}
