#include <iostream>
using namespace std;
void swap(char *a, int i, int j) {
	int s = a[i];
	a[i] = a[j];
	a[j] = s;
}

int length(char* a){
	int i;
	for (i=0;a[i]!=0;++i);
	return i;
}

bool NextSet(char *a, int n)
{
	int j = n - 2;
	while (j != -1 && a[j] >= a[j + 1]) j--; // j - элемент "не на месте"
	if (j == -1)
		return false; // Если всё на месте
	int k = n - 1;
	while (a[j] >= a[k]) k--; // Поиск элемента справа для перестановки
	swap(a, j, k);

	int l = j + 1, r = n - 1; // Перестановка элементов после j в лексиграфическом порядке
	while (l < r)
		swap(a, l++, r--);
	return true;
}

bool check(char* a){
	int i=0;
	while (a[i+1]!=0){
		if (a[i]!=a[i+1])
			return false;
		i++;
	}
	return true;
}

int main()
{	
	char *a = new char[10];int n;
	try{
		cin>>a;
		if (check(a))
			throw 1;
		cin>>n;
		while (NextSet(a, length(a)) && n--){
			cout<<a<<endl;
		}
	}
	catch(...){
		cout<<"bad input"<<endl;
	}
	delete[] a;
	return 0;
}