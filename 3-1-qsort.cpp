#include <iostream>

using namespace std;

void swap(int &n1, int &n2)
{
	int temp = n1;
	n1 = n2;
	n2 = temp;
}

void quickSort(int* a, int n)
{	

	int left = 0;
	int right = n-1;

	int x=a[n>>1]; // Определение опорного элемента
	do
	{
		while (a[left] < x) left++;
		while (a[right] > x) right--;

		if (left <= right)
		{
			swap(a[left], a[right]);
			left++;
			right--;
		}
	} while (left <= right);

	if (right > 0)
		quickSort(a, right+1);
	if (n > left)
		quickSort(a + left, n-left);
}

int main()
{
	int n;
	cin>>n;
	int *a=new int[n];
	for (int i = 0; i < n; ++i)
		cin>>a[i];
	quickSort(a,n);
	for (int i = 0; i < n; ++i)
		cout<<a[i]<<' ';
	delete[] a;
	return 0;
}