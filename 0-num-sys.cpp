#include <iostream>
#include <string.h>
#include <math.h>

const int INPUT_SIZE = 13; // Размер вводимой строки

using namespace std;

void convertToBase(char* result, int number, int base) // Функция перевода целой части к основанию base
{
	int r;
	int k = 0;
	char tmp;
	
    while(number >= 1)
	{
    	r = number % base;
    	if (r > 9 && r < 15)
    		result[k++] = ('A' - 10) + r;
    	else
    		result[k++]= '0' + r;
    	number /= base;
    }
    
    for (int i = 0; i < k / 2; ++i)
	{
    	tmp = result[i];
    	result[i] = result[k - i - 1];
    	result[k - i - 1] = tmp;
    }
    result[k] = '\0';
}

int convertFromBase(int base, char * p) // Функция перевода целой части в десятичную с.с.
{
	if (strlen(p)==0) throw 0; //Проверка наличия целой части

    int result;
    bool neg = false; 
    int ret = 0;          

    if (*p == '-') // Определение знака вводимого числа
    {
        neg = true;
        p++;
    }
    
    for (int i = 0; p[i] != 0; ++i)
    {
        if (p[i] >= '0' && p[i] <= '0'+base-1) 
			result = p[i] - '0';
        else if (p[i] >= 'a' && p[i] <= 'a' + base - 11)
			result = p[i] - ('a' - 10);
        else if (p[i] >= 'A' && p[i] <= 'A' + base - 11)
            result = p[i] - ('A' - 10);
        else throw 0;

        ret = ret * base + result;
    }
    return neg ? -ret : ret;
}

void convertToBase_d(char* result, float number, int base) // Функция перевода дробной части к основанию base
{
	int r = 1;
	int k = 0;
	while (k < 12)
	{
		number *= base;
		r = int(number);
		if (r > 9 && r < 16)
			result[k++]= r + ('A' - 10);
		else
			result[k++]= r + '0';
		number -= r;
	}
}

float convertFromBase_d(int base, char* p ) // Функция перевода дробной части в десятичную с.с.
{
	if (strlen(p)==0) throw 0;
	float result = 0;
	for (int i = 0; p[i] != 0; ++i)
	{
		if (p[i] >= 'A' && p[i] <= 'A' + base - 11)
			result += (p[i] - ('A' - 10)) / pow(base, i + 1);
		else if (p[i] >= 'a' && p[i] <= 'a' + base - 11)
			result += (p[i] - ('a' - 10)) / pow(base, i + 1);
		else if (p[i] >= '0' && p[i] <= '0'+base-1)
			result = result + (p[i] - '0') / pow(base, i + 1);
		else throw 0;
	}
	return	result;
}

void convert(char* x, int b1, char* result, int b2) // Финальная функция перевода из b1 к b2
{
	int t;
	for (t = 0; x[t] != '.' && x[t] != 0; ++t);
	if (x[t] == 0)
		convertToBase(result, convertFromBase(b1, x), b2);
	else
	{
		x[t] = 0;
		convertToBase(result, convertFromBase(b1, x), b2);
		char *tmp = new char[13];
		convertToBase_d(tmp, convertFromBase_d(b1, &x[t + 1]), b2);
		strcat(result, ".");
		strcat(result, tmp);
		delete[] tmp;
	}
}


int main() 
{
	int b1, b2; 
	char x[INPUT_SIZE + 1];
	try 
	{
		cin>>b1>>b2; // Ввод данных
		cin.get();
		cin.getline(x,INPUT_SIZE+1);

		if (b1 < 2 || b1 > 16)
			throw 0;
		if (b2 < 2 || b2 > 16)
			throw 0;
			
		char* str = new char[50];
		convert(x, b1, str, b2);
		cout<<str<<endl;
		delete[] str;
	}
	
	catch(...) 
	{
		cout<<"bad input"<<endl;	
	}

	return 0;
}
