#include <iostream>

using namespace std;

void swap (int &n1, int &n2)
{
	int temp = n1;
	n1 = n2;
	n2 = temp;
}

void heapSort(int *a,int n){
	int left, right;
	int shift = 0; // Смещение "в глубину"
	bool sortable;
	do {           // Приведение к "сортирующего дерева"
		sortable = false; // Флаг возможности дальнейшей сортировки
		for (int i = 0; i < n; ++i) {

			right = 2 * i + 2; // Правый потомок
			left = 2 * i + 1; // Левый потомок
			if (right + shift < n) {
				if ((a[i + shift] >  a[left + shift]) || (a[i + shift] >  a[right + shift])) { // Если "сортирующее дерево" выстроено
					
					if (a[left + shift] <= a[right + shift]) {
						swap(a[i + shift], a[left + shift]);
						sortable = true;
					} else if (a[right + shift] <= a[left + shift]) {
						swap(a[i + shift], a[right + shift]);
						sortable = true;
					}
				}
				if (a[right + shift] <= a[left + shift]) {
					swap(a[left + shift], a[right + shift]);
					sortable = true;
				}
			} else if (left + shift < n) { // Проверка на выход за пределы массива
				if (a[i + shift] >=  a[ left + shift]) {
					swap(a[i + shift], a[left + shift]); // "Поднимаем" меньший элемент в корень
					sortable = true;
				}
			}
		}
		if (!sortable)
			++shift; // Если сортировать уже нечего, сдвигаемся вглубь
	} while (shift + 2 < n);
}


int main ()
{
	int n;
	cin>>n;
	int *a = new int[n];
	for (int i = 0; i < n; ++i)
		cin>>a[i];
	heapSort(a, n);
	for (int i = 0; i < n; ++i)
		cout<<a[i]<<' ';
	delete[] a;
	return 0;
}