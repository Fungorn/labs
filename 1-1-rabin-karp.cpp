#include <iostream>
#include <math.h>
#include <string>
#include <vector>

using namespace std;

int h(const string s){
	int s_len=s.length();
	int result=0;
	for (int i=0;i<s_len;++i){
		result+=(s[i]%3)*pow(3,i);
	}
	return result;
}

void find(string s,string substr){
	int substr_h=h(substr);
	cout<<substr_h<<' ';
	int s_h;string tmp;
	for (int pos_s=0;pos_s<s.length()-substr.length()+1;++pos_s){
		tmp=s.substr(pos_s,substr.length());
		s_h=h(tmp);
		if (s_h==substr_h){
			for (int pos_substr=0;pos_substr<substr.length();++pos_substr){
				if (substr[pos_substr]==tmp[pos_substr])
					cout<<pos_s+1+pos_substr<<' ';
				else
					break;
			}
		}
	}
}

int main(int argc, char const *argv[])
{
	string s,substr;
	getline(cin,substr);
	getline(cin,s);
	find(s,substr);
	return 0;
}